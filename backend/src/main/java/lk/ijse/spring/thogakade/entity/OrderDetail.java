package lk.ijse.spring.thogakade.entity;

import javax.persistence.*;

@Entity
public class OrderDetail {
    private int qty;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "oid",referencedColumnName = "oid",insertable = false,updatable = false)
    private Order order;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "itemcode",referencedColumnName = "itemcode",insertable = false,updatable = false)
    private Item item;
    @EmbeddedId
    private OrderDetail_PK orderDetail_PK;

    public OrderDetail() {
    }

    public OrderDetail(int qty, Order order, Item item, OrderDetail_PK orderDetail_PK) {
        this.setQty(qty);
        this.setOrder(order);
        this.setItem(item);
        this.setOrderDetail_PK(orderDetail_PK);
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public OrderDetail_PK getOrderDetail_PK() {
        return orderDetail_PK;
    }

    public void setOrderDetail_PK(OrderDetail_PK orderDetail_PK) {
        this.orderDetail_PK = orderDetail_PK;
    }

    @Override
    public String toString() {
        return "OrderDetail{" +
                "qty=" + qty +
                ", order=" + order +
                ", item=" + item +
                ", orderDetail_PK=" + orderDetail_PK +
                '}';
    }
}
