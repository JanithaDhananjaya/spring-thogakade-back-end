package lk.ijse.spring.thogakade.service.impl;

import lk.ijse.spring.thogakade.dto.CustomerDTO;
import lk.ijse.spring.thogakade.entity.Customer;
import lk.ijse.spring.thogakade.repositry.CustomerRepositry;
import lk.ijse.spring.thogakade.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class CustomerServieImpl implements CustomerService {

    @Autowired
    private CustomerRepositry customerRepositry;

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void addCustomer(CustomerDTO customerDTO) {
        customerRepositry.save(new Customer(customerDTO.getId(), customerDTO.getName(), customerDTO.getAddress()));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void updateCustomer(String customerId, CustomerDTO customerDTO) {
        if(!customerDTO.getId().equals(customerId)){
            throw new RuntimeException("Customer ID mismatched");
        }
        customerRepositry.save(new Customer(customerDTO.getId(), customerDTO.getName(), customerDTO.getAddress()));
    }

    @Override
    public void deleteCustomer(String customerId) {
        customerRepositry.deleteById(customerId);
    }

    @Override
    public CustomerDTO searchCustomer(String customerId) {
        Customer customer = customerRepositry.findById(customerId).get();
        return new CustomerDTO(customer.getId(), customer.getName(), customer.getAddress());
    }

    @Override
    public List<CustomerDTO> viewAllCustomers() {
        List<Customer> allCustomers = customerRepositry.findAll();
        ArrayList<CustomerDTO> dtos = new ArrayList<>();
        allCustomers.forEach(c -> dtos.add(new CustomerDTO(c.getId(), c.getName(), c.getAddress())));
        return dtos;
    }
}
