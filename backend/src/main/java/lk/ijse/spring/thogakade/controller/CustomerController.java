package lk.ijse.spring.thogakade.controller;

import lk.ijse.spring.thogakade.dto.CustomerDTO;
import lk.ijse.spring.thogakade.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/v1/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public void addCustomer(@RequestBody CustomerDTO customerDTO) {
        customerService.addCustomer(customerDTO);
    }

    @PostMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateCustomer(@PathVariable("id") String customerId, @RequestBody CustomerDTO customerDTO) {
        customerService.updateCustomer(customerId, customerDTO);
    }

    @DeleteMapping("/{id}")
    public void deleteCustomer(@PathVariable("id") String customerId) {
        customerService.deleteCustomer(customerId);
    }

    @GetMapping("/{id}")
    public CustomerDTO searchCustomer(@PathVariable("id") String customerId) {
        return customerService.searchCustomer(customerId);
    }

    @GetMapping
    public List<CustomerDTO> viewAllCustomers() {
        return customerService.viewAllCustomers();
    }

}
