package lk.ijse.spring.thogakade.service.impl;

import lk.ijse.spring.thogakade.dto.ItemDTO;
import lk.ijse.spring.thogakade.dto.OrderDTO;
import lk.ijse.spring.thogakade.dto.OrderDetailDTO;
import lk.ijse.spring.thogakade.entity.*;
import lk.ijse.spring.thogakade.repositry.OrderRepository;
import lk.ijse.spring.thogakade.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(propagation = Propagation.SUPPORTS,readOnly = true)
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;


    @Override
    public boolean placeOrder(OrderDTO orderDTO) {
        List<OrderDetailDTO> orderDetailDTOs = orderDTO.getOrderDetailDTOs();
        Customer customer=new Customer(orderDTO.getCustomerDTO().getId(),orderDTO.getCustomerDTO().getName(),orderDTO.getCustomerDTO().getAddress());

        Item item=null;
        List<OrderDetail> orderDetails = new ArrayList<>();

        Order order =new Order(orderDTO.getOid(),orderDTO.getOrderDate(),orderDetails,customer);

        for (OrderDetailDTO orderDetailDTO : orderDetailDTOs) {
            item=new Item();
            ItemDTO itemDTO = orderDetailDTO.getItemDTO();
            item.setItemcode(itemDTO.getItemcode());
            item.setQtyonhand(itemDTO.getQtyonhand());
            item.setDescription(itemDTO.getDescription());
            item.setUnitprice(itemDTO.getUnitprice());

            OrderDetail orderDetail=new OrderDetail();
            orderDetail.setItem(item);
            orderDetail.setOrder(order);
            orderDetail.setQty(orderDetailDTO.getQty());
            orderDetail.setOrderDetail_PK(new OrderDetail_PK(orderDTO.getOid(),itemDTO.getItemcode()));

            orderDetails.add(orderDetail);
        }

        orderRepository.save(order);

        return true;
    }
}
