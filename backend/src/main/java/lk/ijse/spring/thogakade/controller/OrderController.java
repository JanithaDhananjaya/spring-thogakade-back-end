package lk.ijse.spring.thogakade.controller;

import lk.ijse.spring.thogakade.dto.OrderDTO;
import lk.ijse.spring.thogakade.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.soap.Addressing;

@RestController
@CrossOrigin
@RequestMapping(value = "api/v1/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean plcaeOrder(@RequestBody OrderDTO orderDTO){
        return orderService.placeOrder(orderDTO);
    }

}
