package lk.ijse.spring.thogakade.service.impl;

import lk.ijse.spring.thogakade.dto.ItemDTO;
import lk.ijse.spring.thogakade.entity.Item;
import lk.ijse.spring.thogakade.repositry.ItemRepositry;
import lk.ijse.spring.thogakade.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemRepositry itemRepositry;

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void addItem(ItemDTO itemDTO) {
        itemRepositry.save(new Item(itemDTO.getItemcode(), itemDTO.getDescription(), itemDTO.getUnitprice(), itemDTO.getQtyonhand()));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public void updateItem(String itemcode, ItemDTO itemDTO) {
        if (!itemDTO.getItemcode().equals(itemcode)) {
            throw new RuntimeException("Item Code mismatched!");
        }
        itemRepositry.save(new Item(itemDTO.getItemcode(), itemDTO.getDescription(), itemDTO.getUnitprice(), itemDTO.getQtyonhand()));
    }

    @Override
    public void deleteItem(String itemcode) {
        itemRepositry.deleteById(itemcode);
    }

    @Override
    public ItemDTO searchItem(String itemcode) {
        Item item = itemRepositry.findById(itemcode).get();
        return new ItemDTO(item.getItemcode(), item.getDescription(), item.getUnitprice(), item.getQtyonhand());
    }

    @Override
    public List<ItemDTO> viewAllItems() {
        List<Item> all = itemRepositry.findAll();
        ArrayList<ItemDTO> allItems = new ArrayList<>();
        for (Item item : all) {
            allItems.add(new ItemDTO(item.getItemcode(), item.getDescription(), item.getUnitprice(), item.getQtyonhand()));
        }
        return allItems;
    }
}
