package lk.ijse.spring.thogakade.service;

import lk.ijse.spring.thogakade.dto.ItemDTO;

import java.util.List;

public interface ItemService {

    void addItem(ItemDTO itemDTO);

    void updateItem(String itemcode,ItemDTO itemDTO);

    void deleteItem(String itemcode);

    ItemDTO searchItem(String itemcode);

    List<ItemDTO> viewAllItems();


}
