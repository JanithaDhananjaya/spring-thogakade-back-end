package lk.ijse.spring.thogakade.service;

import lk.ijse.spring.thogakade.dto.CustomerDTO;

import java.util.List;

public interface CustomerService {

    void addCustomer(CustomerDTO customerDTO);

    void updateCustomer(String customerId, CustomerDTO customerDTO);

    void deleteCustomer(String customerId);

    CustomerDTO searchCustomer(String customerId);

    List<CustomerDTO> viewAllCustomers();
}
