package lk.ijse.spring.thogakade.controller;

import lk.ijse.spring.thogakade.dto.ItemDTO;
import lk.ijse.spring.thogakade.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/v1/items")
public class ItemController {

    @Autowired
    private ItemService itemService;

    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public void addItem(@RequestBody ItemDTO itemDTO) {
        itemService.addItem(itemDTO);
    }

    @PostMapping(value = "/{itemcode}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateItem(@PathVariable("itemcode") String itemcode, @RequestBody ItemDTO itemDTO) {
        itemService.updateItem(itemcode, itemDTO);
    }

    @DeleteMapping("/{itemcode}")
    public void deleteItem(@PathVariable("itemcode") String itemcode) {
        itemService.deleteItem(itemcode);
    }

    @GetMapping("/{itemcode}")
    public ItemDTO searchItem(@PathVariable("itemcode") String itemcode) {
        return itemService.searchItem(itemcode);
    }

    @GetMapping
    public List<ItemDTO> viewAllItems() {
        return itemService.viewAllItems();
    }

}
