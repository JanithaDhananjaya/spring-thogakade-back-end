package lk.ijse.spring.thogakade.repositry;

import lk.ijse.spring.thogakade.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepositry extends JpaRepository<Item,String> {
}
