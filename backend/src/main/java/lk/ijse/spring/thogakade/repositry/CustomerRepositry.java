package lk.ijse.spring.thogakade.repositry;

import lk.ijse.spring.thogakade.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepositry extends JpaRepository<Customer,String> {
}
