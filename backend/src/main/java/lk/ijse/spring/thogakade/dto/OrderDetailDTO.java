package lk.ijse.spring.thogakade.dto;

public class OrderDetailDTO {
    private int qty;
    private ItemDTO itemDTO;

    public OrderDetailDTO(int qty, ItemDTO itemDTO) {
        this.setQty(qty);
        this.setItemDTO(itemDTO);
    }

    public OrderDetailDTO() {

    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public ItemDTO getItemDTO() {
        return itemDTO;
    }

    public void setItemDTO(ItemDTO itemDTO) {
        this.itemDTO = itemDTO;
    }
}
