package lk.ijse.spring.thogakade.repositry;

import lk.ijse.spring.thogakade.entity.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, String> {
}
