package lk.ijse.spring.thogakade.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Item {
    @Id
    private String itemcode;
    private String description;
    private double unitprice;
    private int qtyonhand;

    public Item() {
    }

    public Item(String itemcode, String description, double unitprice, int qtyonhand) {
        this.setItemcode(itemcode);
        this.setDescription(description);
        this.setUnitprice(unitprice);
        this.setQtyonhand(qtyonhand);
    }


    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(double unitprice) {
        this.unitprice = unitprice;
    }

    public int getQtyonhand() {
        return qtyonhand;
    }

    public void setQtyonhand(int qtyonhand) {
        this.qtyonhand = qtyonhand;
    }

    @Override
    public String toString() {
        return "Item{" +
                "itemcode='" + itemcode + '\'' +
                ", description='" + description + '\'' +
                ", unitprice=" + unitprice +
                ", qtyonhand=" + qtyonhand +
                '}';
    }
}
