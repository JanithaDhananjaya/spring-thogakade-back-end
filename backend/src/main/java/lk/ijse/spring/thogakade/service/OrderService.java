package lk.ijse.spring.thogakade.service;

import lk.ijse.spring.thogakade.dto.OrderDTO;

public interface OrderService {
    boolean placeOrder(OrderDTO orderDTO);
}
